# Search for Scaling Solutions

## What and Why of Scaling?

Imagine you have come with a simple **program** that can be used to do something meaningful for its users. Initially you are only hosting the program on your system when it has limited users, but gradually more users (*traffic*) start using your program and they even want to pay you for using it. So then the program needs to be hosted on a reliable system that can be accessed as per end-user’s requirement. This is when **scaling** or *scalability* comes into the big picture to handle expected or unexpected traffic. 

**Scalability** is the ability to handle or respond to more user’s requests by buying bigger (**_vertical scaling_**) or more (**_horizontal scaling_**) machines (*systems/services*). These systems/services in modern technological landscape can be executed in two ways either “*on-premise*” or in “*cloud*”. The reason we go for these scaling solutions is manifold, but some crucial points to take into consideration are:
* Configuration
* Settings
* Reliability


## Horizontal vs Vertical scaling

Horizontal and Vertical scaling are similar in a sense that both involve adding computing resources to your initial system’s setup. The important difference lies in terms of implementation and performance. In horizontal scaling, we add more *machines* outside of our initial setup, which is also known as “*scaling out*” in general terms while in vertical scaling, we add more *power* to our existing setup, which is known as “*scaling up*”. There are pros and cons to both kinds of scaling.

### Pros of horizontal scaling
* **Easy scaling from hardware perspective** - We only need to add more machines to our existing system without analyzing their specifications.
* **Less Downtime** - Addition of new systems would not require us to switch-off old systems which would lessen the impact on users.
* **High Resiliency** - Since our resources are distributed across several systems, a faulty system would not impact the end users which would lead to a resilient service.

### Cons of horizontal scaling
* **Complexity of Maintenance and Operation** - Multiple systems require more maintenance. Since there are more than one system, we need to ensure that information flow between these systems is not hindered in any way. One way to ensure this is through **load balancing**. 
* **High Initial Investment** - Adding new systems is expensive as compared to upgrading older ones.

### Pros of vertical scaling
* **Cost Efficiency** - Adding new resources to an existing system is cost effective as it ensures less spending on maintenance and operations.
* **Less Complexity** - It is easier for information to flow within a single system without caring about the synchronicity.

### Cons of vertical scaling
* **More Downtime and Less Resiliency** - A single system is more vulnerable to downtime and points of failure which can potentially lead to high costs and greater impact on end users.
* **Resource Limitation** - A system can only be upgraded to a threshold beyond which it would not be able to work effectively.

![Horizontal vs Vertical scaling](https://www.section.io/assets/images/blog/featured-images/horizontal-vs-vertical-scaling-diagram.png)


---
## Load Balancers

> In computing, load balancing refers to the process of distributing a set of tasks over a set of resources, with the aim of making their overall process more efficient. - _Wikipedia_

As mentioned above load balancers are used to manage systems in scaling. They can be in form of software or hardware that are deployed to manage our existing servers. In common term, we can think of them as traffic cops that are used to effectively manage the information flow across servers so that all of them can run efficiently and there is no blockade at any junction (_system/server_).

![Load balancers](https://www.nginx.com/wp-content/uploads/2014/07/what-is-load-balancing-diagram-NGINX-640x324.png)


---
## References:
* [Section.io blog on Scaling](https://www.section.io/blog/scaling-horizontally-vs-vertically/)
* [Cloudzero.com blog on Horizontal vs Vertical Scaling](https://www.cloudzero.com/blog/horizontal-vs-vertical-scaling)
* [Cloudflare.com blog on Load Balancing](https://www.cloudflare.com/en-in/learning/performance/what-is-load-balancing/)
* [Wikipedia article on Load Balancing](https://en.wikipedia.org/wiki/Load_balancing_(computing))
* [Nginx article for traffic cop analogy](https://www.nginx.com/resources/glossary/load-balancing/)
* Gaurav Sen, [System Design Basics: Horizontal vs. Vertical Scaling](https://www.youtube.com/watch?v=xpDnVSmNFX0)